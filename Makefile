CC=g++
CFLAGS=-Wall -Wextra

all: calc converter
debug:
	mkdir -pv bin/tests
	$(CC) $(CFLAGS) src/simpcalc/main.cpp src/simpcalc/TimeCalc.cpp src/simpcalc/logtrigon.cpp -o bin/simpcalc
	$(CC) $(CFLAGS) src/simpconverter/converter.cpp src/simpconverter/numconv.cpp -o bin/simpconverter
	$(CC) $(CFLAGS) src/tests/checktimecalc.cxx src/simpcalc/TimeCalc.cpp -o bin/tests/checktimecalc
calc:
	mkdir bin
	$(CC) src/simpcalc/main.cpp src/simpcalc/TimeCalc.cpp src/simpcalc/logtrigon.cpp -o bin/simpcalc
converter:
	mkdir bin
	$(CC) src/simpconverter/converter.cpp src/simpconverter/numconv.cpp -o bin/simpconverter
iec-80000-13:
	mkdir -pv bin
	$(CC) src/simpconverter/iec-80000-13.cxx -o bin/iec-80000-13
clean:
	rm -rf bin
