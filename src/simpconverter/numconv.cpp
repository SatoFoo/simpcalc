#include <iostream>
#include "../calc.h"
int dectobin() {
    int num, modulo, bin = 0, var = 1;
    std::cout << "Enter a number: " << std::endl;
    std::cin >> num;
    while (num > 0) {
        modulo = num % 2;
        num = num / 2;
        bin = bin + (modulo * var);
        var = var * 10;
    }
    std::cout << "The binary is " << bin << std::endl;
    return 0;
}
int bintodec() {
    int modulo, num = 0, decnum = 0, tempvar = 1;
    std::cout << "Enter a binary number: \n";
    std::cin >> num;
    while (num > 0) {
        modulo = num % 10;
        num = num / 10;
        decnum += modulo * tempvar;
        tempvar = tempvar * 2;
    }
    std::cout << "The decimal number is " << decnum << std::endl;
    return 0;
}
