#include <cmath>
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <vector>

//TBD: make a part of simpconverter

#define IEC_CONV_VERSION "0.0.2"
#define CALC_VERSION "0.2.1"

double mega2mebi(double inp) {
   return inp / 1.049;
}
double mebi2mega(double inp) {
   return inp * 1.049;
}
double giga2gibi(double inp){
   return inp / 1.074;
}
double gibi2giga(double inp){
   return inp * 1.074;
}
double kilo2kibi(double inp){
   return inp / 1.024;
}
double kibi2kilo(double inp){
   return inp * 1.024;
}

double mega2kilo(double inp){
   return inp * 1000;
}
double mebi2kibi(double inp){
   return inp * 1000;
}
double giga2mega(double inp){
   return inp * 1000;
}
double gibi2mebi(double inp){
   return inp * 1024;
}

double giga2kilo(double inp){
   return inp * pow(10, 6);
}
double gibi2kibi(double inp){
   return inp * (1.049 * pow(10, 6));
}
double kilo2giga(double inp){
   return inp / pow(10, 6);
}
double kilo2mega(double inp){
   return inp / 1000;
}
double kibi2gibi(double inp){
   return inp / (1.049 * pow(10, 6));
}

double mega2kibi(double inp){
   return inp * 976.6;
}
double giga2kibi(double inp){
   return inp * 976600;
}
double mebi2kilo(double inp){
   return inp * 1049;
}
double gibi2kilo(double inp){
   return inp * (1.074 * (pow(10,6)));
}


double conv(std::string orig, std::string target, double inp) {
   double out;
   if (orig == "MB") {
       if (target == "MiB") {
           out = mega2mebi(inp);
       }
       if ((target == "kB") || (target == "KB") || (target == "kb")) {
           out = mega2kilo(inp);
       }
       if (target == "KiB") {
           out = mega2kibi(inp);
       }
   }
   if (orig == "MiB") {
       if (target == "MB") {
           out = mebi2mega(inp);
       }
       if (target == "KiB") {
           out = mebi2kibi(inp);
       }
   }
   if (orig == "GB") {
       if (target == "GiB") {
           out = giga2gibi(inp);
       }
       if (target == "MB") {
           out = giga2mega(inp);
       }
       if (target == "kB") {
           out = giga2kilo(inp);
       }
       if (target == "KiB") {
           out = giga2kibi(inp);
       }
   }
   if (orig == "GiB") {
       if (target == "GB") {
           out = gibi2giga(inp);
       }
       if (target == "MiB") {
           out = gibi2mebi(inp);
       }
       if (target == "KiB") {
           out = gibi2kibi(inp);
       }
   }
   if (orig == "kB") {
       if (target == "KiB") {
           out = kilo2kibi(inp);
       }
       if (target == "MB") {
           out = kilo2mega(inp);
       }
       if (target == "GB") {
           out = kilo2giga(inp);
       }
   }
   if (orig == "KiB") {
       if (target == "kB") {
           out = kibi2kilo(inp);
       }
   }

   if (orig == "kbit") {
       if (target == "Kibit") {
           out = kilo2kibi(inp);
       }
   }
   if (orig == "Kibit") {
       if (target == "kbit") {
           out = kibi2kilo(inp);
       }
   }
   if (orig == "Mbit") {
       if (target == "Mibit") {
           out = mega2mebi(inp);
       }
       if (target == "Kibit") {
           out = mega2kibi(inp);
       }
   }
   if (orig == "Mibit") {
       if (target == "Mbit") {
           out = mebi2mega(inp);
       }
       if (target == "kbit") {
           out = mebi2kilo(inp);
       }
   }
   if (orig == "Gbit") {
       if (target == "Gibit") {
           out = giga2gibi(inp);
       }
   }
   if (orig == "Gibit") {
       if (target == "Gbit") {
           out = gibi2giga(inp);
       }
       if (target == "kbit") {
           out = gibi2kilo(inp);
       }
   }

   return out;
}
std::string fixinput(std::string strinp){
   std::string fix;
   if (strinp == "mbit") {
       fix = "Mbit";
   }
   if (strinp == "gb") {
       fix = "GB";
   }
   if (strinp == "mb") {
       fix = "MB";
   }
   if (strinp == "mib") {
       fix = "MiB";
   }
   if ((strinp == "kb") || (strinp == "KB")) {
       fix = "kB";
   } else {
       fix = strinp;
   }
   return fix;
}

int conv_iec_main() {
   std::string orig, target;
   double inp, out;
   std::cout << "Supported measurement units: Mbit, GB, MB, MiB, kB, kbit, Gibit, Mibit, Kibit, KiB, GiB" << std::endl;
   std::cout << "Enter value to convert:" << std::endl;
   std::cin >> inp;
   std::cout << "Enter original measurement unit:" << std::endl;
   std::cin >> orig;
   std::cout << "Enter target measurement unit:" << std::endl;
   std::cin >> target;
   if (inp != 0) {
       if (!orig.empty()) {
           if (!target.empty()) {
               std::string fix_orig=fixinput(orig);
               std::string fix_targ=fixinput(target);

               if (!fix_orig.empty()) {
                   if (!fix_targ.empty()) {
                       out = conv(fix_orig, fix_targ, inp);
                   } else {
                       out = conv(fix_orig, target, inp);
                   }
               }
               if (!fix_targ.empty()) {
                   if (fix_orig.empty()) {
                       out = conv(orig, fix_targ, inp);
                   }
               }
               if (fix_orig.empty()) {
                   if (fix_targ.empty()) {
                       out = conv(orig, target, inp);
                   }
               }
               std::cout << "Output: " << out << std::endl;
           }
       }
   }

   return 0;
}

double conv_shell_inp(std::string val, std::string inpmeasure_unit) {
   std::string targ;
   double out;
   double inpval = std::stod(val);

   std::cout << "Enter the target measurement unit:\n";
   std::cin >> targ;
   out = conv(fixinput(inpmeasure_unit), fixinput(targ), inpval);
   std::cout << "Output: " << out << std::endl;
   return 0;
}
int main(int argc, char *argv[]) {
   if (argc > 1) {
       std::vector<std::string> iec_args(argv, argv+argc);
       for (size_t r = 1;r < iec_args.size(); r++) {
           // if ((iec_args[r] == "h") || (iec_args[r] == "help")|| (argc == 0)) {
           //     help();
           // }
           if (iec_args[1] == "c") {
               conv_shell_inp(iec_args[2], iec_args[3]);
           }
           if (iec_args[1] == "v") {
           std::cout << "simpcalc version " << CALC_VERSION << std::endl;
           std::cout << "iec_converter version " << IEC_CONV_VERSION << std::endl;
       }
       }
       } else {
           conv_iec_main();
       }
       return 0;
   }

