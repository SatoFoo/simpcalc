#include <iostream>
#include <cstdio>
#include <ctime>
#include "../calc.h"
int fixhrs(int hrs) {
    if (hrs > 23) {
        hrs = hrs - 24;
    }
    return hrs;
}
int fixmins(int mins) {
    if (mins > 59) {
        mins = mins - 60;
    }
    return mins;
}
int fixsecs(int secs) {
    if (secs > 59) {
        secs = secs - 60;
    }
    return secs;
}
int chktimestdcompliance(int hours, int mins, int secs, int nday) {
    if (fixsecs(secs) != secs) {
        secs = fixsecs(secs);
        mins++;
        if (fixmins(mins) != mins) {
        mins = fixmins(mins);
        hours++;
        if (fixhrs(hours) != hours) {
        hours = fixhrs(hours);
        nday++;
        }
        }
    } else {
        if (fixmins(mins) != mins) {
        mins = fixmins(mins);
        hours++;
        if (fixhrs(hours) != hours) {
        hours = fixhrs(hours);
        nday++;
    }
    }
    }
    if (fixhrs(hours) != hours) {
        hours = fixhrs(hours);
        nday++;
    }
    if (nday > 0) {
        if (nday = 1) {
            printf("The result is Tomorrow, %02d : %02d : %02d \n", hours, mins, secs);
        } else {
            printf("The result is %02d : %02d : %02d \n", hours, mins, secs);
        }
    } else {
        printf("The result is %02d : %02d : %02d \n", hours, mins, secs);
    }
    return 0;
}
int TimeCalc() {
    int hours,minutes,seconds, rhours, rmins, rsecs;
    int nday = 0;
    std::time_t curr_time;
    curr_time = time(NULL);
    
    std::tm *tm_local = std::localtime(&curr_time);
    int curr_hour = tm_local->tm_hour;
    int curr_min = tm_local->tm_min;
    int curr_sec = tm_local->tm_sec;
    printf("Current time is %02d : %02d : %02d \n", curr_hour, curr_min, curr_sec);
    std::cout << "Enter amount of hours to add or subtract (negative numbers to subtract):\n";
    std::cin >> hours;
    std::cout << "Enter amount of minutes to add or subtract (negative numbers to subtract):\n";
    std::cin >> minutes;
    std::cout << "Enter amount of seconds to add or subtract (negative numbers to subtract):\n";
    std::cin >> seconds;
    rhours = curr_hour+hours;
    rmins = curr_min+minutes;
    rsecs = curr_sec+seconds;
    chktimestdcompliance(rhours, rmins, rsecs, nday);
    return 0;
}
